# USB-C (2.0) to otter board


<img width="1200px" alt="schematic" src="usbc to led.svg">

<img width="1200px" alt="pcb" src="pcb.png">

## used
- [otter silkscreen from Jana-Marie](https://github.com/Jana-Marie/KiCAD-libs/blob/master/otter.pretty/otter_small3.kicad_mod)
- [JLCPCB template](https://github.com/sethhillbrand/kicad_templates/tree/master/JLCPCB_1-2Layer)
## configurations
### solder bridge
must be connected to **either**:
- gnd: receives power
- 5V: provides power
### resistors
R1 and R2 should be set according to the max current used or provided. See the [Hackaday Article: All About USB-C: Resistors And Emarkers](https://hackaday.com/2023/01/04/all-about-usb-c-resistors-and-emarkers/) for more details.

R3 and R4 should be near 0Ω for usb communication or higher for "non standart" communication.
